FROM python:3

WORKDIR /usr/src/app

# This block install libspartialindex-dev
# libspatialindex-dev is requeried by geopandas
ADD http://ftp.us.debian.org/debian/pool/main/s/spatialindex/libspatialindex3_1.8.1-3_amd64.deb /var/cache/apt/archives
ADD http://ftp.us.debian.org/debian/pool/main/s/spatialindex/libspatialindex-c3_1.8.1-3_amd64.deb /var/cache/apt/archives
ADD http://ftp.us.debian.org/debian/pool/main/s/spatialindex/libspatialindex-dev_1.8.1-3_amd64.deb /var/cache/apt/archives
RUN cd /var/cache/apt/archives && dpkg -i libspatialindex3_1.8.1-3_amd64.deb
RUN cd /var/cache/apt/archives && dpkg -i libspatialindex-c3_1.8.1-3_amd64.deb
RUN cd /var/cache/apt/archives && dpkg -i libspatialindex-dev_1.8.1-3_amd64.deb
RUN cd /var/cache/apt/archives && rm *.deb
# -- END


RUN pip install cython
RUN git clone https://github.com/jswhit/pyproj /usr/local/src/pyproj
RUN cd /usr/local/src/pyproj
RUN pip install -r /usr/local/src/pyproj/requirements-dev.txt
RUN cd /usr/local/src/pyproj && pip install .

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

RUN git clone https://github.com/geopandas/geopandas.git /usr/local/src/geopandas
RUN cd /usr/local/src/geopandas && pip install .

COPY . .

CMD [ "python", "./main.py" ]
